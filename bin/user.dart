bool anyUserUnder18(Iterable<User> users) {
  return users.any((user) => user.age < 18);
}

bool anyUserUnder13(Iterable<User> users) {
  return users.any((user) => user.age > 13);
}

void main() {
  var users = [
    User(name: 'ABCDE', age: 14),
    User(name: 'BCDEF', age: 18),
    User(name: 'CDE', age: 18),
    User(name: 'DEF', age: 21),
    User(name: 'E', age: 25)

  ];
  if (anyUserUnder18(users)) {
    print('Have any user under 18');
  }
  if (anyUserUnder13(users)) {
    print('every user is over 13');
  }
}

class User {
  String name;
  int age;
  User({required this.name, required this.age});
  @override
  String toString() {
    return '$name,$age';
  }

}
